
const express = require('express');
const bodyParser = require('body-parser')
const app = express();

const port = '3000'

app.use(bodyParser.urlencoded({ extended: true }))

app.use(bodyParser.json())

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});



app.post('/', (req,res)=>{
  console.log(req.body)
  let json = {
    "oi": "OI"
  }
  res.send(json)
})

app.listen(port, ()=>{
  console.log(`Server listen on port ${port}`);
})
